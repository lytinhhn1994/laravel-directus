<?php

namespace DBizPackages\LaravelDirectus;

use DateTime;
use DateTimeZone;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\Grammar as GrammarBase;
use Illuminate\Support\Facades\Config;
use RuntimeException;

class Grammar extends GrammarBase
{
    private $config = [];

    /**
     * @param array $config
     * @return Grammar
     */
    public function setConfig(array $config): self
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param Builder $query
     * @return string|false
     */
    public function compileSelect(Builder $query)
    {
        // Get params.
        $table = $query->from;
        $typeSql = str_contains($table, "/");
        $filters = "";
        $select = "";

        $sort = '[';
        $type = "gql";
        $single_page = str_contains($table, Config::get("database.connections.directus.single_page_prefix"));
        if ($typeSql) {
            $type = "restApi";
            $url = $query->connection->getDatabaseName() . $table;
            return array("query" => $query, 'url' => $url, "type" => $type);
        }
        if ($single_page) {
            $type = "single_page";
            $table = str_replace(Config::get("database.connections.directus.single_page_prefix"), "", $table);
            $url = $query->connection->getDatabaseName() . "/items/{$table}";
            return array("query" => $query, 'url' => $url, "type" => $type);
        }
        if (!empty($query->orders)) {
            foreach ($query->orders as $order) {
                if ($order['direction'] === 'desc') {
                    $sort .= "\"-{$order['column']}\",";
                } else {
                    $sort .= "\"{$order['column']}\",";
                }
            }
        } else {
            $sort .= '"-date_created"';
        }
        $sort .= "]";
        if ($query->limit) {
            $limit = $query->limit;
        } else {
            $limit = -1;
        }

        if ($query->offset) {
            $offset = $query->offset;
        } else {
            $offset = 0;
        }

        if (!empty($query->columns)) {
            foreach ($query->columns as $column) {
                $select .= "{$column},";
            }

        } else {
            $select = "id";
        }

        foreach ($query->wheres as $where) {
            // Get key and strip table name.
            $key = $where['column'];
            // Check where type.
            switch ($where['type']) {
                case 'Basic':
                    switch ($where['operator']) {
                        case '=':
                            $param = "{$key}_eq";
                            break;
                        case '>=':
                            $param = "{$key}_gte";
                            break;

                        case '>':
                            $param = "{$key}_gt";
                            break;

                        case '<=':
                            $param = "{$key}_lte";
                            break;

                        case '<':
                            $param = "{$key}_lt";
                            break;
                        case '!=':
                            $param = "{$key}_neq";
                            break;
                        case 'LIKE':
                            $param = "{$key}_contains";
                            break;

                        default:
                            throw new RuntimeException('Unsupported query where operator ' . $where['operator']);
                    }
                    $params[$param] = $this->filterKeyValue($key, $where['value']);
                    break;

                case 'In':
                    $params["{$key}_in"] = $this->filterKeyValue($key, $where['values']);
                    break;

                case 'NotIn':
                    $params["{$key}_nin"] = $this->filterKeyValue($key, $where['values']);
                    break;

                case 'InRaw':
                    $params[$key] = $this->filterKeyValue($key, $where['values']);
                    break;

                case 'between':
                    $params["{$key}_between"] = [$this->filterKeyValue($key, $where['values'][0]), $this->filterKeyValue($key, $where['values'][1])];
                    break;
                // Ignore the following where types.
                case 'NotNull':
                    $params["{$key}_nnull"] = $this->filterKeyValue($key, $where['boolean']);
                    break;

                case 'Null':
                    $params["{$key}_null"] = $this->filterKeyValue($key, $where['boolean']);
                    break;

                default:
                    throw new RuntimeException('Unsupported query where type ' . $where['type']);
            }
        }

        if (!empty($query->aggregate) && $query->aggregate["function"] == "count") {
            $type = "count";
            $select = "id";
            $limit = -1;
            $offset = 0;
        }
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                if (strrpos($key, '.') !== false) {
                    $column = explode('.', $key);
                    $tabChild = array_shift($column);
                    $condition = explode('_', array_pop($column));
                    $conditionFiled = current($condition);
                    $conditionSymbol = end($condition);
                    $column = !empty($column) ? current($column) : null;
                    $filters .= "{$tabChild}: {";
                    $filters .= !empty($column)
                        ?
                        "{$column} : {
                            {$conditionFiled}: {
                                            _{$conditionSymbol}: " . (is_bool($value) ? ($value ? "true" : "false") : (is_array($value) ? json_encode($value) : (is_numeric($value) ? $value : "\"{$value}\""))) . "
                                        }
                        }
                        "
                        :

                        "{$conditionFiled}: {
                                        _{$conditionSymbol}: " . (is_bool($value) ? ($value ? "true" : "false") : (is_array($value) ? json_encode($value) : (is_numeric($value) ? $value : "\"{$value}\""))) . "
                                    }";
                    $filters .= "},";
                } else {
                    $columnCondition = explode('_', $key);
                    $condition = array_pop($columnCondition);
                    $column = implode("_",$columnCondition);
                    $filters .= "{$column}: { _{$condition}: " . (is_bool($value) ? ($value ? "true" : "false") : (is_array($value) ? json_encode($value) : (is_numeric($value) ? $value : "\"{$value}\""))) . " },";
                }
            }

        } else {
            $filters = null;
        }

        $dQuery = <<<GQL
                query {
                    $table(filter: {
                            $filters
                        },
                        limit: $limit,
                        offset: $offset,
                        sort: $sort
                    ) {
                       $select
                    }
                }
    GQL;
        return array("query" => $query, 'dQuery' => $dQuery, "type" => $type);
    }

    /**
     * @param string $key
     * @param string|array|integer|null $value
     * @return mixed
     */
    private function filterKeyValue($key, $value)
    {
        // Convert timezone.
        $connTimezone = $this->config['timezone'] ?? null;
        if ($connTimezone && in_array($key, $this->config['datetime_keys'])) {
            $connDtZone = new DateTimeZone($connTimezone);
            $appDtZone = new DateTimeZone(config('app.timezone'));
            if (is_string($value)) {
                if (strlen($value) === 19) {
                    $value = (new DateTime($value, $appDtZone))->setTimezone($connDtZone)->format('Y-m-d H:i:s');
                }
            } else if (is_array($value)) {
                $value = array_map(function ($value) use ($connDtZone, $appDtZone) {
                    if (is_string($value) && strlen($value) === 19) {
                        $value = (new DateTime($value, $appDtZone))->setTimezone($connDtZone)->format('Y-m-d H:i:s');
                    }
                    return $value;
                }, $value);
            }
        }

        return $value;
    }

    /**
     * Compile an insert statement into SQL.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $values
     * @return string
     */
    public function compileInsert(Builder $query, array $values)
    {
        // Essentially we will force every insert to be treated as a batch insert which
        // simply makes creating the SQL easier for us since we can utilize the same
        // basic routine regardless of an amount of records given to us to insert.
        $table = $this->wrapTable($query->from);

//        if (!is_array(reset($values))) {
//            $values = [$values];
//        }

        return [
            'url' => "/" . $query->from,
            'data' => $values
        ];
    }


    /**
     * Compile an update statement into SQL.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $values
     * @return array
     */
    public function compileUpdate(Builder $query, array $values)
    {
        return [
            'url' => $query->connection->getDatabaseName() . "/" . $query->from,
            'data' => $values
        ];
    }

    /**
     * Compile an delete statement into SQL.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @return array
     */
    public function compileDelete(Builder $query)
    {
        return [
            'select' => $this->compileSelect($query),
            'url' => "/" . $query->from
        ];
    }

}
