<?php

namespace DBizPackages\LaravelDirectus;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Database\Connection as ConnectionBase;
use Illuminate\Database\Grammar as GrammerBase;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use RuntimeException;

class Connection extends ConnectionBase
{
    const AUTH_TYPE_TOKEN = 'token';

    /**
     * @return GrammerBase
     */
    protected function getDefaultQueryGrammar()
    {
        $grammar = app(Grammar::class);
        $grammar->setConfig($this->getConfig());

        return $this->withTablePrefix($grammar);
    }


    /**
     * @param string|false $query E.g. /articles?status=published
     * @param mixed[] $bindings
     * @param bool $useReadPdo
     * @return mixed[]
     */
    public function select($query, $bindings = [], $useReadPdo = true)
    {
        // Check query.
        if (!$query) {
            return [];
        }
        if ($query["type"] == "single_page") {
            return $this->run($query["url"], $bindings, function ($query) {
                // Get full URL.
                $json = $this->getJsonByUrl($query);
                if (empty($json["error"])) {
                    $data = json_decode($json["success"]);
                    return $data;
                } else {
                    \Log::info($json["error"]);
                    return [];
                }
            });
        } elseif ($query["type"] == "restApi") {
            return $this->run($query["url"], $bindings, function ($query) {
                // Get full URL.
                $json = $this->getJsonByUrl($query);
                if (empty($json["error"])) {
                    $data = json_decode($json["success"]);
                    return $data;
                } else {
                    \Log::info($json["error"]);
                    return [];
                }
            });
        } else {
            return $this->run($query["dQuery"], $bindings, function ($query) {
                $url = $this->getDatabaseName() . "/graphql";
                // Get full URL.
                $json = $this->postJsonByUrl($url, $query);
                if (empty($json["error"])) {
                    $data = json_decode($json["success"])->data;
                    return current($data);
                } else {
                    \Log::info($json["error"]);
                    return [];
                }

            });

        }


    }

    /**
     * Run an insert statement against the database.
     *
     * @param string $query
     * @param array $bindings
     * @return bool
     */
    public function insert($query, $bindings = [])
    {
        $fullUrl = $this->getDatabaseName() . $query['url'];
        foreach (current($query['data']) as $data) {
            if (str_contains($query['url'], "files") && is_file($data)) {
                try {

                    $multipart = [
                        "filename" => $data->getClientOriginalName(),
                        "contents" => $data->get(),
                        "name" => $data->getBasename()
                    ];

                    $file = $this->createClient(false)->post($fullUrl, [
                        "multipart" => [$multipart]
                    ])->getBody()->getContents();
                    $return['success'] = json_decode($file)->data->id;
                } catch (ClientException $exception) {
                    $return['error'] = $exception->getResponse()->getBody()->getContents();
                }
                return $return;

            } else {
                try {
                    $return = $this->postJsonByUrl($fullUrl, $data);
                } catch (\Exception $exception) {
                    $return = $exception->getResponse()->getBody()->getContents();
                }
            }

        }
        return $return;
    }

    /**
     * Run an update statement against the database.
     *
     * @param string $query
     * @param array $bindings
     * @return int
     */
    public function update($query, $bindings = [])
    {

        return $this->patchJsonByUrl($query['url'], $query['data']);

    }

    /**
     * Run a delete statement against the database.
     *
     * @param string $query
     * @param array $bindings
     * @return int
     */
    public function delete($query, $bindings = [])
    {

        $items = $this->select($query['select'], $bindings);
        $return = [
            'success' => [],
            'error' => [],
        ];
        foreach ($items as $item) {
            try {
                $fullUrl = $this->getDatabaseName() . $query['url'] . "/" . $item['id'];
                $return = $this->deteleJsonByUrl($fullUrl);
            } catch (\Exception $exception) {
                $return['error'][] = $item['id'];
            }
        }

        return $return;
    }


    private function createClient($withJson = true)
    {
        $headers = $this->getConfig('headers') ?: [];
        if ($withJson)
            $headers = ['Content-Type' => 'application/json'];

        $user = Auth::user();
        if ($user && $token = $user->getToken()) {
            if (!empty($token)) {
                $headers = array_merge($headers, ['Authorization' => 'Bearer ' . $user->getToken()]);
            }
        }
        $client = new Client(['headers' => $headers]);
        return $client;
    }

    /**
     * @param string $url
     * @return array
     */
    private function getJsonByUrl($url)
    {
        try {
            $return["success"] = $this->createClient()->get($url)->getBody()->getContents();
        } catch (ClientException $exception) {
            $return["error"] = $exception->getResponse()->getBody()->getContents();
        }

        return $return;
    }

    /**
     * @param string $url
     * @return array
     */
    private function putJsonByUrl($url, $data)
    {

        try {

            $return["success"] = $this->createClient()->put($url, $data)->throw()->json();
        } catch (ClientException $exception) {
            $return["error"] = $exception->getResponse()->getBody()->getContents();
        }


        return $return;
    }

    private function patchJsonByUrl($url, $data)
    {

        try {
            $return["success"] = $this->createClient()->patch($url, ["json" => $data])->getBody()->getContents();
        } catch (ClientException $exception) {
            $return["error"] = $exception->getResponse()->getBody()->getContents();
        }


        return $return;
    }

    /**
     * @param string $url
     * @return array
     */
    private function deteleJsonByUrl($url)
    {
        try {

            $return["success"] = $this->createClient()->delete($url)->throw()->json();
        } catch (ClientException $exception) {
            $return["error"] = $exception->getResponse()->getBody()->getContents();
        }


        return $return;
    }

    /**
     * @param string $url
     * @param array $data
     * @return array
     */
    private function postJsonByUrl($url, $data)
    {
        try {

            $return["success"] = $this->createClient()->post($url, ['json' => ['query' => $data]])->getBody()->getContents();
        } catch (ClientException $exception) {
            $return["error"] = $exception->getResponse()->getBody()->getContents();
        }

        return $return;


    }


}

class Builder extends QueryBuilder
{

    public function aggregate($function, $columns = ['*'])
    {

        $results = $this->cloneWithout($this->unions ? [] : ['columns'])
            ->cloneWithoutBindings($this->unions ? [] : ['select'])
            ->setAggregate($function, $columns)
            ->get($columns)->toArray();

        if (!empty($results)) {
            return count($results);
        }


    }
}
